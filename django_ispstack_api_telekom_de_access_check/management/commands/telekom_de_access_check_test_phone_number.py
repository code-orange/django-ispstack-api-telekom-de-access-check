from pprint import pprint

from django.core.management.base import BaseCommand

from django_ispstack_api_telekom_de_access_check.django_ispstack_api_telekom_de_access_check.api_client import (
    access_product_check_by_phone_number,
)


class Command(BaseCommand):
    help = "Test access_product_check_by_phone_number"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        response = access_product_check_by_phone_number(49, 2192, 85490)

        pprint(response)

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
