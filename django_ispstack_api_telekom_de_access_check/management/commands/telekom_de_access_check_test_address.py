from pprint import pprint

from django.core.management.base import BaseCommand

from django_ispstack_api_telekom_de_access_check.django_ispstack_api_telekom_de_access_check.api_client import (
    access_product_check_by_address,
)
from django_mdat_location.django_mdat_location.models import MdatCities, MdatCountries


class Command(BaseCommand):
    help = "Test access_product_check_by_address"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        response = access_product_check_by_address(
            city=MdatCities.objects.get(
                zip_code=42499, country=MdatCountries.objects.get(iso="de")
            ),
            street="Peterstr.",
            house_nr="69",
        )

        pprint(response)

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
