import hashlib
import json
from datetime import timedelta, datetime

import requests
import xmltodict
from django.conf import settings
from zeep import Client
from zeep.transports import Transport

from django_ispstack_access.django_ispstack_access.models import (
    IspAccessCarrierLink,
    IspAccessCheckCache,
)
from django_ispstack_api_telekom_de_main.django_ispstack_api_telekom_de_main.zeep_signature import (
    BinarySignatureNoVerify,
)
from django_mdat_location.django_mdat_location.models import *


def access_product_check_by_address(
    city: MdatCities,
    street: str,
    house_nr: str,
    house_nr_add: str = None,
    district: str = None,
    kls_id: str = None,
):
    # DTAG needs german translation
    city.set_current_language("de")

    check_request = dict()
    check_request["Adresse_Request"] = dict()
    check_request["Adresse_Request"]["Version"] = "000.001"
    check_request["Adresse_Request"]["Adresse"] = dict()
    check_request["Adresse_Request"]["Adresse"]["Strasse"] = street
    check_request["Adresse_Request"]["Adresse"]["Hausnummer"] = house_nr

    if house_nr_add is not None:
        check_request["Adresse_Request"]["Adresse"]["Hausnummernzusatz"] = house_nr_add

    if district is not None:
        check_request["Adresse_Request"]["Adresse"]["Ortsteil"] = district

    if kls_id is not None:
        check_request["Adresse_Request"]["Adresse"]["KLS-ID"] = kls_id

    check_request["Adresse_Request"]["Adresse"]["Postleitzahl"] = city.zip_code
    check_request["Adresse_Request"]["Adresse"]["Ort"] = city.title

    return access_product_check(check_request, "Adressbezogen")


def access_product_check_by_phone_number(
    country_code: int, national_code: int, subscriber_num: int
):
    check_request = dict()
    check_request["Anschluss_Request"] = dict()
    check_request["Anschluss_Request"]["Version"] = "000.001"
    check_request["Anschluss_Request"]["Anschluss"] = dict()
    check_request["Anschluss_Request"]["Anschluss"]["ONKZ"] = national_code
    check_request["Anschluss_Request"]["Anschluss"]["Rufnummer"] = subscriber_num

    return access_product_check(check_request, "Rufnummernbezogen")


def access_product_check(payload: dict, mode="Adressbezogen"):
    hash_json = hashlib.sha256(json.dumps(payload, sort_keys=True).encode("utf8"))
    q_hash = hash_json.hexdigest()

    try:
        result_data_obj = IspAccessCheckCache.objects.get(
            q_hash=q_hash,
            carrier_id=2,
            date_added__gt=datetime.now() - timedelta(hours=24),
        )
    except IspAccessCheckCache.DoesNotExist:
        use_cache = False
    else:
        result_data = result_data_obj.content
        use_cache = True

    if not use_cache:
        tk_proxy = (
            settings.TELEKOM_DE_ACCESS_CHECK_API_PROXY_HOST
            + ":"
            + str(settings.TELEKOM_DE_ACCESS_CHECK_API_PROXY_PORT)
        )

        proxies = {"http": tk_proxy, "https": tk_proxy}

        rsession = requests.session()
        rsession.proxies.update(proxies)

        # No globally trusted CA
        rsession.verify = False

        transport = Transport(session=rsession)

        client = Client(
            "django_ispstack_api_telekom_de_access_check/django_ispstack_api_telekom_de_access_check/schema/Verfuegbarkeit.wsdl",
            wsse=BinarySignatureNoVerify(
                settings.TELEKOM_DE_ACCESS_CHECK_CERT_FILE_KEY,
                settings.TELEKOM_DE_ACCESS_CHECK_CERT_FILE_PUB,
            ),
            transport=transport,
        )

        # Workaround to fix static WSDL
        client.service._binding_options["address"] = (
            settings.TELEKOM_DE_ACCESS_CHECK_API_URL
        )

        with client.settings(strict=False, raw_response=True):
            if mode == "Adressbezogen":
                result = client.service.sucheMarktproduktOperation(
                    Adressbezogen=payload
                )
            elif mode == "Rufnummernbezogen":
                result = client.service.sucheMarktproduktOperation(
                    Rufnummernbezogen=payload
                )
            else:
                raise ValueError

        result_data = xmltodict.parse(result.content)

        # cleanup cache
        try:
            cleanup = IspAccessCheckCache.objects.get(
                q_hash=q_hash,
                carrier_id=2,
            )
        except IspAccessCheckCache.DoesNotExist:
            pass
        else:
            cleanup.delete()

        result_data_obj = IspAccessCheckCache(
            q_hash=q_hash,
            carrier_id=2,
            content=result_data,
            date_added=datetime.now(),
        )
        result_data_obj.save()

    available_products = list()

    try:
        product_list = result_data["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][
            "sucheMarktprodukt"
        ]["Adressbezogen"]["Adresse_Response"]["DSL-Kapazitaet"]["Produktliste"]
    except KeyError:
        return available_products

    for product in product_list:
        available_products.extend(
            IspAccessCarrierLink.objects.filter(
                carrier_id=2,
                carrier_article_nr=product["Materialnummer"],
                order_enabled=True,
                subproduct__order_enabled=True,
            )
        )

    return list(dict.fromkeys(available_products))
